solution "BOTB-WebBuzzer-Server"
	configurations {"Debug", "Release"}
	uuid "04fd0620-8e8f-11ea-ab12-0800200c9a66"
	
	project "BOTB-WebBuzzer-Server"
		location "build"
		debugdir ""
		language "C++"
		files {"src/**.h", "src/**.cpp"}
		flags {"EnableSSE", "Unicode", "Symbols"}
		kind "ConsoleApp"
		uuid "202059c0-8e8f-11ea-ab12-0800200c9a66"
				
		configuration "windows"
			defines {"_CRT_SECURE_NO_WARNINGS"}
			if os.is("windows") then vpaths { ["*"] = "src" } end
		
		configuration "gmake"
			buildoptions_cpp { '-std=c++11', '-gdwarf-2' }
					
		configuration "Debug"
			defines {"_DEBUG"}
			targetdir "Debug"
			objdir "Debug_obj"
			
		configuration "Release"
			defines {"NDEBUG"}
			targetdir "Release"
			flags {"OptimizeSpeed"}
			objdir "Release_obj"
			
		configuration {"vs*", "Release"}
			buildoptions { "/GL /openmp" } --whole program optimization, openMP support
			linkoptions {"/LTCG"} --link-time code generation


#include "packet.h"

#include <cstring>

union int8 {
	uint8_t u;
	int8_t s;
};

union int16 {
	uint16_t u;
	int16_t s;
};

union int32 {
	uint32_t u;
	int32_t s;
};

const std::string Packet::getMessage() const {
	std::string str;
	if (buffer.size() > 0)
		str.assign(&buffer[0], buffer.size());
	return str;
}

void Packet::addUint8(uint8_t data) {
	int8 conv;
	conv.u = data;
	buffer.push_back(conv.s);
}

void Packet::addInt8(int8_t data) {
	buffer.push_back(data);
}

void Packet::addUint32(uint32_t data) {
	int32 conv;
	conv.u = data;
	buffer.push_back(conv.s);
	buffer.push_back(conv.s >> 8);
	buffer.push_back(conv.s >> 16);
	buffer.push_back(conv.s >> 24);
}

void Packet::addInt32(int32_t data) {
	buffer.push_back(data);
	buffer.push_back(data >> 8);
	buffer.push_back(data >> 16);
	buffer.push_back(data >> 24);
}

void Packet::addFloat32(float data) {
	char floatBytes[4];
	memcpy(floatBytes, &data, 4);
	buffer.push_back(floatBytes[0]);
	buffer.push_back(floatBytes[1]);
	buffer.push_back(floatBytes[2]);
	buffer.push_back(floatBytes[3]);
}

void Packet::addFloat64(double data) {
	char floatBytes[8];
	memcpy(floatBytes, &data, 8);
	buffer.push_back(floatBytes[0]);
	buffer.push_back(floatBytes[1]);
	buffer.push_back(floatBytes[2]);
	buffer.push_back(floatBytes[3]);
	buffer.push_back(floatBytes[4]);
	buffer.push_back(floatBytes[5]);
	buffer.push_back(floatBytes[6]);
	buffer.push_back(floatBytes[7]);
}

void Packet::addString(std::string &data) {
	// prepend uint32 length (for clientside offset purposes), then string follows (no null termination)
	addUint32(data.length());
	std::copy(data.begin(), data.end(), std::back_inserter(buffer));
}

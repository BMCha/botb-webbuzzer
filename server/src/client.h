#pragma once

#include <string>
#include <vector>

class Client {
	public:
		// the length of our ping list. Ping is averaged over the times in the list
		// the averaging window is thus (once the list is up to full size) this/pinging frequency
		static const unsigned int MAX_PING_TIMES = 20;

		enum class ClientType {
			Lobby = 0,
			Student,
			Faculty,
			QuestionReader
		};

		// the websockets client ID used for communications with this client
		int id;
		// whether or not the client is currently connected to the server
		bool isConnected;
		// the session id for this client
		std::string sessionID;
		// the time that the player lost connection (used to implement a kick timeout)
		double disconnectionTime;

		// average (see addPing impl for averaging method) RTT time (seconds) between client and server
		double ping;
		// time that we should send a ping request to the client
		double nextPingTime;
		// used to both delay the clock sync and also to limit how long we spend syncing clocks
		double clockSyncStartTime;
		// true if we've synchronized the clocks (the client knows what the offset between its clock and ours is)
		bool isClockSynced;

		ClientType type;
		// only set for Student and Faculty types
		std::string name;
		// how many times this client has attempted to auth into a role
		unsigned passwordAttempts;
		// whether or not the client is authed and ready for their role
		bool isReady;

		Client();

		// adds a ping time (RTT in seconds) to the list and updates the average (public double ping)
		void addPing(double argPingTime);

		std::string getRoleName();

	private:		
		// ping list (circular insert with max length after which we just overwrite the oldest value)
		double pingTimes[MAX_PING_TIMES];
		// current insertion index
		int pingIdx;
		// list length
		int numPingTimes;

		// used to calculate median and stuff to get a better ping value
		std::vector<double> sortedPings;
};

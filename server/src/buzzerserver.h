#pragma once

#include <vector>

#include "websockets/websocket.h"
#include "client.h"

// forward declaration so we don't have to include the actual class just for an argument
class Packet;

struct BuzzIn {
	int clientID;
	// i.e. team ID
	Client::ClientType clientType;
	// as given or calculated. The server time of the user input
	double buzzTime;

	// used to tell the receipt order of the buzz-ins. starts at 0 for each sequence and increases from there
	unsigned rxOrder;
};

class BuzzerServer {
	public:
		void run();

	private:		
		// used to calculate deltas in the periodic handler
		double lastPeriodicTime;

		webSocket serverSocket;

		// key is clientID
		std::map<int, Client*> clients;

		// client IDs
		std::vector<int> lobbyClientIDs;
		std::vector<int> studentContestantIDs;
		std::vector<int> facultyContestantIDs;
		std::vector<int> questionReaderClientIDs;

		// the client ID assigned to the next disconnected client (so we don't have issues with the reused websocket client IDs)
		int nextDisconnectedClientID = -1;

		// game state
		unsigned currentBuzzSequenceNumber;
		std::vector<BuzzIn> currentBuzzIns;
		bool wasBuzzCalled;

		void socketOpenHandler(int argClientID);
		void socketCloseHandler(int argClientID);
		void socketMessageHandler(int argClientID, std::string argMessageString);
		void socketPeriodicHandler();

		void sendPacket(int argClientID, Packet &argPacket);
		void broadcastPacket(std::vector<int> &argClientIDs, Packet &argPacket);
		void broadcastPacket(Packet &argPacket, bool argOnlyToReady = false);

		void processMessage(int argClientID, std::string argMessage);

		void sendPing(int argClientID);
		void printPings();

		std::string generateSessionID();
		std::vector<int>& getClientVectorForClient(Client &argClient);
		void renameClient(Client &argClient, int argNewClientID);
		void kickClient(Client &argClient, bool argWasManualDisconnect);

		void sendGamestateToNewClient(Client &argClient, bool argSkipOwnClient);

		void newGame();
		void resetBuzzersForNewSequence();
		double getMaxContestantPing();
		void onBuzzWinnerFound();
};

#pragma once

#include <string>
#include <vector>
#include <cstdint>

class Packet {
	public:
		const std::string getMessage() const;
		void addUint8(uint8_t data);
		void addInt8(int8_t data);
		void addUint32(uint32_t data);
		void addInt32(int32_t data);
		void addFloat32(float data);
		void addFloat64(double data);
		void addString(std::string &data);
	
	protected:
		std::vector<char> buffer;
};

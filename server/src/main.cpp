/*
	CHC BOTB Buzzers		
	Brian Charles
	May 2020
*/

#include <cstdio>

#include "buzzerserver.h"

#if defined(_WIN32)
int wmain(int argc, wchar_t *argv[]) {
#else
int main(int argc, char *argv[]) {
#endif
	printf("BOTB WebBuzzer Server starting...\n");

	BuzzerServer server;
	server.run();

	return EXIT_SUCCESS;
}

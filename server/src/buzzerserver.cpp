#include <iostream>
#include <string>
#include <algorithm>
#include <queue>
#include <cstring>

#include "websockets/sha1.h"

#include "packet.h"
#include "client.h"
#include "timer.h"

#include "buzzerserver.h"

#include "serverconfig.h"

enum messageType {	
	MSG_PING = 1, // IN/OUT
	MSG_PONG, // IN/OUT
	MSG_CLOCKSYNC, // IN/OUT

	MSG_DISCONNECT, // IN/OUT
	MSG_RECONNECT, // IN/OUT
	MSG_JOINROLE, // IN
	MSG_AUTHSTATUS, // OUT

	MSG_BUZZERSTATUS, // OUT
	MSG_BUZZIN, // IN
	MSG_BUZZEDIN, // OUT
	MSG_BUZZWINNER, // OUT

	MSG_RESETBUZZERS, // IN

	MSG_ADDCONTESTANT, // OUT
	MSG_REMOVECONTESTANT, // OUT
	MSG_UPDATECONTESTANTSTATE, // OUT

	MSG_KICKCLIENT, // IN
};

// overload BuzzIn less operator so we can use std::sort
bool operator<(const BuzzIn &argLhs, const BuzzIn &argRhs) {
	return argLhs.buzzTime < argRhs.buzzTime;
}

// opens the websocket and starts listening for connections
// this method doesn't return until the program ends
// all logic will be handled in the various socket handlers
void BuzzerServer::run() {
	newGame();

	// set event handlers
    serverSocket.setOpenHandler(std::bind(&BuzzerServer::socketOpenHandler, this, std::placeholders::_1));
    serverSocket.setCloseHandler(std::bind(&BuzzerServer::socketCloseHandler, this, std::placeholders::_1));
    serverSocket.setMessageHandler(std::bind(&BuzzerServer::socketMessageHandler, this, std::placeholders::_1, std::placeholders::_2));
	serverSocket.setPeriodicHandler(std::bind(&BuzzerServer::socketPeriodicHandler, this));

	// run periodic handler at least ~60hz (value is in uS. default if unset is 10k uS == 0.01 s)
	serverSocket.setSelectTimeout(16666);
	lastPeriodicTime = -1;

	// starts the server and contains a run loop that will keep the program open, calling various websocket event handlers as needed
    serverSocket.startServer(WEBSOCKETS_PORT, true);
	// !!!NOTHING CAN GO BELOW HERE!!! SEE THE COMMENT ABOVE
}

// called when a client connects
void BuzzerServer::socketOpenHandler(int argClientID) {
	std::cout << "[Info] Connection from client: " << argClientID << std::endl;
		
	Client *client = new Client;
	client->id = argClientID;
	client->isConnected = true;
	client->type = Client::ClientType::Lobby;
	clients.insert({client->id, client});

	lobbyClientIDs.push_back(client->id);

	client->nextPingTime = getCurrentSeconds();

	// begin clock synchronization after 2 seconds so we have a good ping history
	client->clockSyncStartTime = getCurrentSeconds() + 2;
}

// called when a client connection is closed
void BuzzerServer::socketCloseHandler(int argClientID) {
	std::cout << "[Info] Client " << argClientID << " disconnected" << std::endl;
	
	// if the client's not in the client map we've already handled their disconnection
	auto clientIt = clients.find(argClientID);
	if (clientIt != clients.end()) {
		// lets assume this is unexpected and keep the client around, just mark as disconnected
		Client &client = *(clients[argClientID]);
		client.isConnected = false;
		client.disconnectionTime = getCurrentSeconds();
		client.isClockSynced = false;
	
		// unless of course they're just sitting in the lobby. In that case we can remove them
		if (client.type == Client::ClientType::Lobby) {
			lobbyClientIDs.erase(std::remove(lobbyClientIDs.begin(), lobbyClientIDs.end(), argClientID), lobbyClientIDs.end());

			delete clients[argClientID];
			clients.erase(argClientID);
		} else {
			// "rename" the client while they're disconnected
			renameClient(client, nextDisconnectedClientID--);

			// inform everyone of the (hopefully) temporary disconect
			if (client.type == Client::ClientType::Student || client.type == Client::ClientType::Faculty) {
				Packet contestantStatePacket;
				contestantStatePacket.addUint8(MSG_UPDATECONTESTANTSTATE);
				contestantStatePacket.addInt32(argClientID);
				contestantStatePacket.addUint8(0);
				contestantStatePacket.addInt32(client.id);
				broadcastPacket(contestantStatePacket, true);
			}
		}
	}
}

// called when a client sends a message to the server
void BuzzerServer::socketMessageHandler(int argClientID, std::string argMessageString) {
	processMessage(argClientID, argMessageString);
}

// called every time select() happens on the socket end (so either a network thing happens or we hit the select timeout)
// we've set it to timeout in ~16.6ms so it'll run at 60+ Hz
void BuzzerServer::socketPeriodicHandler() {
	double now = getCurrentSeconds();

	// handle delta time calc and clamping
	if (lastPeriodicTime < 0) {
		lastPeriodicTime = now;
	}
	double deltaTime = now - lastPeriodicTime;	
	deltaTime = std::max(0.0, std::min(0.25, deltaTime));

	static std::queue<Client*> clientsToKick;
	for (const auto &clientEntry : clients) {
		Client &client = *(clientEntry.second);

		// ping them if it's time
		if (client.nextPingTime <= now) {
			client.nextPingTime = now + (1.0 / PING_FREQUENCY);

			sendPing(client.id);
		}

		// start clock sync if it's time
		if (lastPeriodicTime < client.clockSyncStartTime && client.clockSyncStartTime <= now) {
			Packet clockSyncPacket;
			clockSyncPacket.addUint8(MSG_CLOCKSYNC);
			clockSyncPacket.addUint8(0);
			clockSyncPacket.addFloat64(0);
			sendPacket(client.id, clockSyncPacket);
		}

		// kick if they've been disconnected too long
		if (client.isConnected == false && (now - client.disconnectionTime) > KICK_AFTER_DISCONNECT_TIME) {
			clientsToKick.push(&client);
		}
	}
	// we have to do the actual kicking out here otherwise we'll mess up the client iteration
	while (!clientsToKick.empty()) {
		Client &kickee = *(clientsToKick.front());
		clientsToKick.pop();

		printf("[Info] Autokicking disconnected client: past timeout threshold.\n");
		kickClient(kickee, false);
	}

	// check to see if it's time to call a buzz winner
	if (!wasBuzzCalled && !currentBuzzIns.empty()) {
		// are we done waiting for new buzz-ins?
		double timeSinceEarliestBuzz = now - currentBuzzIns[0].buzzTime;
		// we'll call an answer if the earliest buzz was over maxping*1.2 in the past (which is a 120% margin for recieving buzzes, as ~theoretically~ the slowest tx would be maxPing/2)
		if (timeSinceEarliestBuzz > (getMaxContestantPing() * 1.2)) {
			// we've found our winner!
			onBuzzWinnerFound();
		}
	}

	// used for debugging clocksync
	//printf("\r%10.3f", getCurrentSeconds());

	lastPeriodicTime = now;
}

// send a packet to a specific client
void BuzzerServer::sendPacket(int argClientID, Packet &argPacket) {
	serverSocket.wsSend(argClientID, argPacket.getMessage(), true);
}

// send a message to all connected clients in the provided client IDs vector
void BuzzerServer::broadcastPacket(std::vector<int> &argClientIDs, Packet &argPacket) {
	std::string message = argPacket.getMessage();
	for (const int clientID : argClientIDs) {
		auto clientIt = clients.find(clientID);
		if (clientIt != clients.end()) {
			const Client &client = *((*clientIt).second);
			if (client.isConnected) {				
				serverSocket.wsSend(client.id, message, true);
			}
		}
	}
}

// send a message to all connected clients
// if argOnlyToReady (defaults false) is set, only connected and authed clients get the message
void BuzzerServer::broadcastPacket(Packet &argPacket, bool argOnlyToReady) {
	std::string message = argPacket.getMessage();
	for (const auto &clientEntry : clients) {
		const Client &client = *(clientEntry.second);

		bool shouldSendToClient = client.isConnected;
		if (argOnlyToReady) {
			shouldSendToClient &= client.isReady;
		}

		if (shouldSendToClient) {
			serverSocket.wsSend(client.id, message, true);
		}
	}
}

#define CHECKMESSAGELENGTH_EXACTLY(x) if (bufferLength != (x)) { std::cout << "[Warning] invalid message length (" << bufferLength << ") for message ID: " << (int)messageID << ". Expected " << (x) << "." << std::endl; break; }
#define CHECKMESSAGELENGTH_ATLEAST(x) if (bufferLength < (x)) { std::cout << "[Warning] invalid message length (" << bufferLength << ") for message ID: " << (int)messageID << ". Expected >= " << (x) << "." << std::endl; break; }

// process message from client
void BuzzerServer::processMessage(int argClientID, std::string argMessage) {
	Client &client = *(clients[argClientID]);
	// TODO-IMPROVEMENT: figure out whether I want to use client.id or argClientID in this method and make things consistent! (probably go with client.id)

	size_t bufferLength = argMessage.length();
	char *rawBuffer = new char[bufferLength];	
	memcpy(rawBuffer, argMessage.c_str(), bufferLength);
	char messageID = rawBuffer[0];

	char *buffer = &(rawBuffer[1]);
	bufferLength--;
	
	switch (messageID) {
		// *** Networking Messages ***
		case MSG_PING: {
			CHECKMESSAGELENGTH_EXACTLY(8);
			double startTime;
			memcpy(&startTime, &buffer[0], 8);
			Packet pongPacket;
			pongPacket.addUint8(MSG_PONG);
			pongPacket.addFloat64(startTime);
			sendPacket(argClientID, pongPacket);
			} break;

		case MSG_PONG: {
			CHECKMESSAGELENGTH_EXACTLY(8);
			double startTime;
			memcpy(&startTime, &buffer[0], 8);
			client.addPing(getCurrentSeconds() - startTime);
			//printPings();
			} break;

		case MSG_CLOCKSYNC: {
			CHECKMESSAGELENGTH_EXACTLY(8);
			double guess;
			memcpy(&guess, &buffer[0], 8);
			// add ping/2 (remember to convert to ms) to get it up to "now"
			guess += ((client.ping * 1000) / 2);
			double error = (getCurrentSeconds() * 1000.0) - guess;
			//printf("[Trace] client %d clock sync err: %.3f ms\n", client.id, error);

			bool shouldTryAgain = (std::abs(error) > CLOCK_SYNC_THRESHOLD_MS);
			if ((getCurrentSeconds() - client.clockSyncStartTime) > CLOCK_SYNC_MAX_TIME) {
				shouldTryAgain = false;
				printf("[Warning] client %d hit clock sync time limit of %.3f seconds. Force-completing sync with an error of %.3f ms.\n", client.id, CLOCK_SYNC_MAX_TIME, error);
			}

			if (shouldTryAgain) {
				Packet clockSyncPacket;
				clockSyncPacket.addUint8(MSG_CLOCKSYNC);
				clockSyncPacket.addUint8(0);
				clockSyncPacket.addFloat64(error);
				sendPacket(argClientID, clockSyncPacket);
			} else {
				Packet clockSyncPacket;
				clockSyncPacket.addUint8(MSG_CLOCKSYNC);
				clockSyncPacket.addUint8(1);
				clockSyncPacket.addFloat64(error);
				sendPacket(argClientID, clockSyncPacket);

				if (!client.isClockSynced) {
					// this only runs for the initial clock sync
					printf("[Debug] client %d completed clock sync with an error of %.3f ms (their ping is %.3fms).\n", client.id, error, client.ping * 1000);

					client.isClockSynced = true;				
				} else {
					// completed clock (re)sync
				}
			}

			// schedule a resync
			client.clockSyncStartTime = getCurrentSeconds() + CLOCK_RESYNC_INTERVAL;
			} break;

		// *** Connecting/Disconnecting/Auth Messages ***
		case MSG_DISCONNECT: {
			// this client wants to disconnect
			kickClient(client, true);
		} break;

		case MSG_RECONNECT: {
			CHECKMESSAGELENGTH_ATLEAST(4);
			uint32_t sessionIDLength;
			memcpy(&sessionIDLength, &buffer[0], 4);
			CHECKMESSAGELENGTH_EXACTLY(4 + sessionIDLength);
			std::string sessionID;
			sessionID.assign(&buffer[4], sessionIDLength);

			// check disconnected clients for this session id
			bool reconnectSuccessful = false;
			for (const auto &clientEntry : clients) {
				Client &existingClient = *(clientEntry.second);

				if (!existingClient.isConnected && existingClient.sessionID == sessionID) {
					// swap out the client objects
					int oldClientID = existingClient.id;
					int newClientID = client.id;

					// erase the data for the reconnecting client
					lobbyClientIDs.erase(std::remove(lobbyClientIDs.begin(), lobbyClientIDs.end(), newClientID), lobbyClientIDs.end());
					// rename the existing client that we're reinhabiting to use the new connection ID
					// first we have to remove the entry for the "current" client otherwise the reinsert part won't work
					clients.erase(newClientID);
					renameClient(existingClient, newClientID);

					// we can't rebind references so we have to stop using "client" (which we just deleted anyhow) in this scope
					// use "existingClient" which is the client we're re-inhabiting

					// tell everyone about the reconnect (if this is a contestant)
					if (existingClient.type == Client::ClientType::Student || existingClient.type == Client::ClientType::Faculty) {
						Packet contestantStatePacket;
						contestantStatePacket.addUint8(MSG_UPDATECONTESTANTSTATE);
						contestantStatePacket.addInt32(oldClientID);
						contestantStatePacket.addUint8(1);
						contestantStatePacket.addInt32(existingClient.id);
						broadcastPacket(contestantStatePacket, true);
					}

					existingClient.isClockSynced = true;
					existingClient.isConnected = true;
					existingClient.isReady = true;
					Packet authStatusPacket;
					authStatusPacket.addUint8(MSG_AUTHSTATUS);
					authStatusPacket.addUint8(1);
					authStatusPacket.addInt32(existingClient.id);
					authStatusPacket.addUint8((int)existingClient.type);
					authStatusPacket.addString(existingClient.sessionID);
					sendPacket(existingClient.id, authStatusPacket);
					
					printf("[Info] client %d reconnected in role %s. Formerly known as client %d.\n", newClientID, existingClient.getRoleName().c_str(), oldClientID);
					sendGamestateToNewClient(existingClient, false);
					reconnectSuccessful = true;
					break;
				}
			}
			if (!reconnectSuccessful) {
				printf("[Info] reconnect attempt failed for client %d. No matching session found (possible timeout).\n", client.id);
				Packet authStatusPacket;
				authStatusPacket.addUint8(MSG_AUTHSTATUS);
				authStatusPacket.addUint8(0);
				sendPacket(client.id, authStatusPacket);
			}
			} break;

		case MSG_JOINROLE: {
			CHECKMESSAGELENGTH_ATLEAST(1 + 4 + 1);
			Client::ClientType requestedType = (Client::ClientType)buffer[0];
			bool needsName = (requestedType == Client::ClientType::Student || requestedType == Client::ClientType::Faculty);

			unsigned curOffset = 1;

			uint32_t passwordLength;
			memcpy(&passwordLength, &buffer[curOffset], 4); curOffset += 4;			
			std::string password;
			CHECKMESSAGELENGTH_ATLEAST(curOffset + passwordLength);
			password.assign(&buffer[curOffset], passwordLength);
			curOffset += passwordLength;			

			std::string name;			
			if (needsName) {
				CHECKMESSAGELENGTH_ATLEAST(curOffset + 4);
				uint32_t nameLength;
				memcpy(&nameLength, &buffer[curOffset], 4); curOffset += 4;
				CHECKMESSAGELENGTH_EXACTLY(curOffset + nameLength);
				name.assign(&buffer[curOffset], nameLength);
				curOffset += nameLength;
			}

			bool validPassword = false;
			switch (requestedType) {
				case Client::ClientType::Student:
					validPassword = (password == STUDENT_PASSWORD);
					break;
				case Client::ClientType::Faculty:
					validPassword = (password == FACULTY_PASSWORD);
					break;
				case Client::ClientType::QuestionReader:
					validPassword = (password == QUESTIONREADER_PASSWORD);
					break;
			}
			client.passwordAttempts++;

			if (client.passwordAttempts > MAX_PASSWORD_ATTEMPTS) {
				serverSocket.wsClose(argClientID);
				break;
			}

			// send back their auth status
			client.isReady = validPassword;
			if (validPassword) {
				client.name = name;

				// move client into their role
				client.type = requestedType;

				lobbyClientIDs.erase(std::remove(lobbyClientIDs.begin(), lobbyClientIDs.end(), argClientID), lobbyClientIDs.end());

				getClientVectorForClient(client).push_back(argClientID);
			}

			if (validPassword) {
				client.sessionID = generateSessionID();
			}

			Packet authStatusPacket;
			authStatusPacket.addUint8(MSG_AUTHSTATUS);
			authStatusPacket.addUint8(validPassword ? 1 : 0);
			if (validPassword) {
				authStatusPacket.addInt32(client.id);
				authStatusPacket.addUint8((int)client.type);
				authStatusPacket.addString(client.sessionID);
			}
			sendPacket(argClientID, authStatusPacket);

			if (validPassword) {
				printf("[Info] client %d authorized in role %s.\n", client.id, client.getRoleName().c_str());
				sendGamestateToNewClient(client, true);

				if (client.type == Client::ClientType::Student || client.type == Client::ClientType::Faculty) {
					Packet addContestantPacket;
					addContestantPacket.addUint8(MSG_ADDCONTESTANT);
					addContestantPacket.addInt32(client.id);
					addContestantPacket.addUint8((int)client.type);
					addContestantPacket.addString(client.name);
					broadcastPacket(addContestantPacket, true);
				}							
			}
			} break;

		// *** Game Messages ***
		case MSG_BUZZIN: {
			CHECKMESSAGELENGTH_EXACTLY(12);
			double givenBuzzTime;
			memcpy(&givenBuzzTime, &buffer[0], 8);
			uint32_t buzzSequenceNumber;
			memcpy(&buzzSequenceNumber, &buffer[8], 4);

			if (buzzSequenceNumber != currentBuzzSequenceNumber) {
				// incorrect sequence num (generally a buzz that tried to come in while we were resetting)
				printf("[Warning] client %d buzzed in with sequence %i. Expected sequence %u.\n", client.id, buzzSequenceNumber, currentBuzzSequenceNumber);
				break;
			}

			double buzzTime = givenBuzzTime;

			double estimatedBuzzTime = getCurrentSeconds() - (client.ping / 2);
			double errorBetweenGivenAndEstimatedTime = estimatedBuzzTime - givenBuzzTime;
			double maxAllowedError = (client.ping + (CLOCK_SYNC_THRESHOLD_MS / 1000.0));
			if (errorBetweenGivenAndEstimatedTime > maxAllowedError) {
				printf("[Warning] client %d had a buzz time that was %.2f ms faster than our estimate. Using estimate... (sync threshold is +/- %.2f ms, their ping is %.2f ms)\n", client.id, errorBetweenGivenAndEstimatedTime * 1000, CLOCK_SYNC_THRESHOLD_MS, client.ping * 1000);
				
				// use the estimate and schedule them an immediate resync
				buzzTime = estimatedBuzzTime;
				client.clockSyncStartTime = getCurrentSeconds() + 0.00001;
			}			

			// to avoid weirdness in extreme situations make sure that buzz-ins arriving after the call don't mess with the order too much
			if (wasBuzzCalled) {
				double earliestBuzzTime = currentBuzzIns[0].buzzTime;
				if (buzzTime <= earliestBuzzTime) {
					printf("[Warning] client %d had late buzz in with very odd time. Overriding...\n", client.id);
					buzzTime = std::max(getCurrentSeconds(), earliestBuzzTime + 0.0001);
				}
			}

			printf("[Debug] Client %d buzzed in!\n", client.id);

			// tell everyone about this buzz-in
			Packet buzzedInPacket;
			buzzedInPacket.addUint8(MSG_BUZZEDIN);
			buzzedInPacket.addInt32(client.id);
			buzzedInPacket.addFloat64(buzzTime);
			broadcastPacket(buzzedInPacket, true);			

			currentBuzzIns.push_back({client.id, client.type, buzzTime, (unsigned)currentBuzzIns.size()});
			std::sort(currentBuzzIns.begin(), currentBuzzIns.end(), std::less<BuzzIn>());			
			// buzz winner will be figured out by the periodic handler
			} break;

		case MSG_RESETBUZZERS: {
			CHECKMESSAGELENGTH_EXACTLY(0);
			if (client.type != Client::ClientType::QuestionReader) {
				printf("[Warning] client %d attempted to reset buzzers but they are not a question reader!\n", client.id);
				break;
			}

			resetBuzzersForNewSequence();
			} break;

		case MSG_KICKCLIENT: {
			CHECKMESSAGELENGTH_EXACTLY(4);
			if (client.type != Client::ClientType::QuestionReader) {
				printf("[Warning] client %d attempted to kick someone but they are not a question reader!\n", client.id);
				break;
			}
			int32_t kickeeClientID;
			memcpy(&kickeeClientID, &buffer[0], 4);

			auto clientIt = clients.find(kickeeClientID);
			if (clientIt != clients.end()) {
				printf("[Info] Questionreader %d kicking client...\n", client.id);
				kickClient(*((*clientIt).second), false);
			} else {
				printf("[Warning] cannot kick client %d. Client not found!\n", kickeeClientID);
			}

			} break;

		default:
			std::cout << "[Warning] Unknown message ID: " << messageID << std::endl;
	}

	delete[] rawBuffer;
}

// send ping to player (called when we get a ping from them -- let the clients do the timing for us)
void BuzzerServer::sendPing(int argClientID) {
	Packet pingPacket;
	pingPacket.addUint8(MSG_PING);
	pingPacket.addFloat64(getCurrentSeconds());
	sendPacket(argClientID, pingPacket);
}

void BuzzerServer::printPings() {
	printf("\r [Trace] Pings: ");

	for (const auto &clientEntry : clients) {
		const Client &client = *(clientEntry.second);
		if (client.isConnected)
			printf("[%d] : %.0fms  ", client.id, client.ping * 1000.0);
		else
			printf("[%d] --:--ms  ", client.id);
	}
}

// a super hacky session id generator that spits out ~random~ SHA1s
static const char* hexDigitTable = "0123456789abcdef";
std::string BuzzerServer::generateSessionID() {
	double inputDouble = getCurrentSeconds();
	inputDouble *= (rand() / (double)RAND_MAX);

	unsigned char *sha1MD = new unsigned char[SHA1_DIGEST_LENGTH];
	SHA1((unsigned char*)&inputDouble, sizeof(double), sha1MD);
	char *sha1ASCII = new char[SHA1_DIGEST_LENGTH * 2];
	for (int i = 0; i < SHA1_DIGEST_LENGTH; i++) {
		unsigned char byte = sha1MD[i];
		sha1ASCII[i * 2] = hexDigitTable[(byte & 0xf0) >> 4];
		sha1ASCII[(i * 2) + 1] = hexDigitTable[byte & 0x0f];
	}

	std::string sha1String = std::string(sha1ASCII, SHA1_DIGEST_LENGTH * 2);
	delete[] sha1MD;
	delete[] sha1ASCII;

	return sha1String;
}

std::vector<int>& BuzzerServer::getClientVectorForClient(Client &argClient) {
	switch (argClient.type) {
		case Client::ClientType::Lobby:
		default:
			return lobbyClientIDs;

		case Client::ClientType::Student:
			return studentContestantIDs;

		case Client::ClientType::Faculty:
			return facultyContestantIDs;

		case Client::ClientType::QuestionReader:
			return questionReaderClientIDs;
	}
}

void BuzzerServer::renameClient(Client &argClient, int argNewClientID) {
	int oldID = argClient.id;
	argClient.id = argNewClientID;

	clients.erase(oldID);
	clients.insert({argNewClientID, &argClient});

	for (int &clientID : studentContestantIDs) {
		if (clientID == oldID) clientID = argNewClientID;
	}
	for (int &clientID : facultyContestantIDs) {
		if (clientID == oldID) clientID = argNewClientID;
	}
	for (int &clientID : questionReaderClientIDs) {
		if (clientID == oldID) clientID = argNewClientID;
	}

	for (BuzzIn &buzz : currentBuzzIns) {
		if (buzz.clientID == oldID) buzz.clientID = argNewClientID;
	}
}

// perhaps slightly misnamed. This is more of a "when the client needs to be removed", whether that's initiated by us or them
// argWasManualDisconnect means it's initiated by them and thus not really a kick, if that makes sense
void BuzzerServer::kickClient(Client &argClient, bool argWasManualDisconnect) {
	// save this since we're going to delete the client
	int clientID = argClient.id;

	// fix the client ID on any buzz-ins from this client (unless they've already been renamed i.e. have a negative ID)
	if (clientID >= 0) {
		renameClient(argClient, nextDisconnectedClientID--);
	}	

	Client::ClientType type = argClient.type;
	std::vector<int>& clientVector = getClientVectorForClient(argClient);
	clientVector.erase(std::remove(clientVector.begin(), clientVector.end(), argClient.id), clientVector.end());

	// actually disconnect them
	if (argClient.isConnected) {
		serverSocket.wsClose(clientID);
	}

	delete clients[clientID];
	clients.erase(clientID);	

	// tell everyone this player is gone (if they were a contesant)
	if (type == Client::ClientType::Student || type == Client::ClientType::Faculty) {
		Packet removeContestantPacket;
		removeContestantPacket.addUint8(MSG_REMOVECONTESTANT);
		removeContestantPacket.addInt32(clientID);
		broadcastPacket(removeContestantPacket, true);
	}

	if (argWasManualDisconnect == false) {
		printf("[Info] Kicked client %d from the server.\n", clientID);
	}
}

void BuzzerServer::sendGamestateToNewClient(Client &argClient, bool argSkipOwnClient) {
	// get them the current sequence number
	Packet buzzerStatusPacket;
	buzzerStatusPacket.addUint8(MSG_BUZZERSTATUS);
	buzzerStatusPacket.addUint8(1);
	buzzerStatusPacket.addUint32(currentBuzzSequenceNumber);
	sendPacket(argClient.id, buzzerStatusPacket);

	// get them all the players (excluding themselves if they're a contestant -- we just sent that to them)
	unsigned numContestants = studentContestantIDs.size() + facultyContestantIDs.size();
	for (unsigned i = 0; i < numContestants; i++) {
		int contestantID = (i < studentContestantIDs.size()) ? studentContestantIDs[i] : facultyContestantIDs[i - studentContestantIDs.size()];
		if (argSkipOwnClient && (contestantID == argClient.id)) {
			continue;
		}

		Client &contestant = *(clients[contestantID]);

		Packet addContestantPacket;
		addContestantPacket.addUint8(MSG_ADDCONTESTANT);
		addContestantPacket.addInt32(contestant.id);
		addContestantPacket.addUint8((int)contestant.type);
		addContestantPacket.addString(contestant.name);
		sendPacket(argClient.id, addContestantPacket);

		// if the client is inactive send along a status packet for them
		if (!contestant.isConnected) {
			Packet contestantStatePacket;
			contestantStatePacket.addUint8(MSG_UPDATECONTESTANTSTATE);
			contestantStatePacket.addInt32(contestant.id);
			contestantStatePacket.addUint8(0);
			contestantStatePacket.addInt32(contestant.id);
			sendPacket(argClient.id, contestantStatePacket);
		}
	}

	// send along any active buzz-ins
	for (BuzzIn &buzz : currentBuzzIns) {
		Packet buzzedInPacket;
		buzzedInPacket.addUint8(MSG_BUZZEDIN);
		buzzedInPacket.addInt32(buzz.clientID);
		buzzedInPacket.addFloat64(buzz.buzzTime);
		sendPacket(argClient.id, buzzedInPacket);
	}

	// and the winner, if called
	if (wasBuzzCalled) {
		const BuzzIn &winningBuzz = currentBuzzIns[0];

		Packet buzzWinnerPacket;
		buzzWinnerPacket.addUint8(MSG_BUZZWINNER);
		buzzWinnerPacket.addInt32(winningBuzz.clientID);
		buzzWinnerPacket.addUint8((int)winningBuzz.clientType);
		sendPacket(argClient.id, buzzWinnerPacket);
	}
}

void BuzzerServer::newGame() {
	currentBuzzSequenceNumber = 0;
	resetBuzzersForNewSequence();
}

void BuzzerServer::resetBuzzersForNewSequence() {
	currentBuzzIns.clear();
	wasBuzzCalled = false;

	currentBuzzSequenceNumber++;

	Packet buzzerEnablePacket;
	buzzerEnablePacket.addUint8(MSG_BUZZERSTATUS);
	buzzerEnablePacket.addUint8(1);
	buzzerEnablePacket.addUint32(currentBuzzSequenceNumber);

	broadcastPacket(buzzerEnablePacket, true);
}

double BuzzerServer::getMaxContestantPing() {
	double maxPing = 0;

	for (int clientID : studentContestantIDs) {
		Client &client = *(clients[clientID]);
		maxPing = std::max(maxPing, client.ping);
	}

	for (int clientID : facultyContestantIDs) {
		Client &client = *(clients[clientID]);
		maxPing = std::max(maxPing, client.ping);
	}

	return maxPing;
}

void BuzzerServer::onBuzzWinnerFound() {
	wasBuzzCalled = true;

	const BuzzIn &winningBuzz = currentBuzzIns[0];

	Packet buzzWinnerPacket;
	buzzWinnerPacket.addUint8(MSG_BUZZWINNER);
	buzzWinnerPacket.addInt32(winningBuzz.clientID);
	buzzWinnerPacket.addUint8((int)winningBuzz.clientType);
	broadcastPacket(buzzWinnerPacket, true);
}

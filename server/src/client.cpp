#include "client.h"

#include <algorithm>

Client::Client() :
		id(-1),
		isConnected(false),
		disconnectionTime(-1),
		ping(0),
		nextPingTime(-1),
		clockSyncStartTime(-1),
		isClockSynced(false),
		pingIdx(0),
		numPingTimes(0),
		sortedPings(MAX_PING_TIMES),
		type(Client::ClientType::Lobby),
		name(""),
		passwordAttempts(0),
		isReady(false) {
	// empty constructor
}

void Client::addPing(double argPingTime) {
	pingTimes[pingIdx] = argPingTime;
	pingIdx++;
	if (numPingTimes < MAX_PING_TIMES)
		numPingTimes++;
	if (pingIdx >= MAX_PING_TIMES)
		pingIdx = 0;

	sortedPings.assign(pingTimes, pingTimes + numPingTimes);
	// ascending sort
	std::sort(sortedPings.begin(), sortedPings.end());
	
	double median = sortedPings[numPingTimes / 2];
	double mean = 0;
	double trimmedMean = 0;
	unsigned numElementsInTrimmedMean = 0;
	float trimmedMeanPercentage = 0.8f; // use the center 80%

	for (int i = 0; i < numPingTimes; i++) {
		double &pingTime = sortedPings[i];
		mean += pingTime;

		float trimBorderWidth = (1 - trimmedMeanPercentage) / 2;
		if (i >= std::floor(numPingTimes * trimBorderWidth) && i < std::ceil(numPingTimes * (1 - trimBorderWidth))) {
			trimmedMean += pingTime;
			numElementsInTrimmedMean++;
		}
	}
	mean /= sortedPings.size();
	trimmedMean /= numElementsInTrimmedMean;

	// TODO-IMPROVEMENT: while this is presumably better than a simple mean we should figure out what the best method to use is here. Do we want to do some sort of stddev check and just throw out outliers? etc etc?
	ping = trimmedMean;
}

std::string Client::getRoleName() {
	switch (type) {
		case ClientType::Lobby:
		default:
			return "Lobby";

		case ClientType::Student:
			return "Student";

		case ClientType::Faculty:
			return "Faculty";

		case ClientType::QuestionReader:
			return "QuestionReader";
	};
}
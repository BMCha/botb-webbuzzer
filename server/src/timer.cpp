#include "timer.h"

#include <cstddef>

#if defined(__linux__) || defined(__APPLE__)
#include <sys/time.h>

double getCurrentSeconds() {
	timeval curTime;
	gettimeofday(&curTime, NULL);
	return curTime.tv_sec + (static_cast<double>(curTime.tv_usec) / 1000000.0);
}

//////////////////////////////////////////////////////////////////

#elif defined(_WIN32)
#include <Windows.h>

double getCurrentSeconds() {
	LARGE_INTEGER curTicks, ticksPerSecond;
	QueryPerformanceCounter(&curTicks);
	QueryPerformanceFrequency(&ticksPerSecond);
	return static_cast<double>(curTicks.QuadPart) / static_cast<double>(ticksPerSecond.QuadPart);
}

#endif

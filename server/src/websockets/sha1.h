#ifndef SHA1_H
#define SHA1_H

const size_t SHA1_DIGEST_LENGTH = 20;

unsigned char *
SHA1(const unsigned char *d, size_t n, unsigned char *md);

#endif /* SHA1_H */

#pragma once
// The configurable parameters of buzzerserver, broken out into this separate file to make local edits easier

#include <string>

// clock synchronization will continue until the client's clock is within this many ms of ours
const double CLOCK_SYNC_THRESHOLD_MS = 10;
// ...or until we've spent this much time (in seconds) trying to get it there
const double CLOCK_SYNC_MAX_TIME = 5;
// we will resync active users clocks every <this many> seconds
const double CLOCK_RESYNC_INTERVAL = 15;

// Hz
const double PING_FREQUENCY = 10;

// after a user is unexpectedly disconnected for this many seconds we will auto-kick them
const float KICK_AFTER_DISCONNECT_TIME = 15;

const int WEBSOCKETS_PORT = 1988; // first year of CHP :P
const int MAX_PASSWORD_ATTEMPTS = 5;

const std::string STUDENT_PASSWORD = "student";
const std::string FACULTY_PASSWORD = "faculty";
const std::string QUESTIONREADER_PASSWORD = "questionreader";
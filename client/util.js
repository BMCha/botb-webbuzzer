var animate = window.requestAnimationFrame || 
	window.webkitRequestAnimationFrame || 
	window.mozRequestAnimationFrame || 
	function(callback) { window.setTimeout(callback, 1000/60) };

var getTimestamp;
if (window.performance.now) {
	console.log("util::getTimestamp -- Using high performance timer");
	getTimestamp = function() { return window.performance.now(); };
} else if (window.performance.webkitNow) {
	console.log("util::getTimestamp -- Using webkit high performance timer");
	getTimestamp = function() { return window.performance.webkitNow(); };
} else {
	console.log("util::getTimestamp -- Using low performance timer");
	getTimestamp = function() { return new Date().getTime(); };
}

function lerp(a, b, factor) {
	return a + (factor * (b - a));
}

// returns new append offset
function addUTF8StringToDataViewAtOffset(argString, argDataView, argStartOffset) {
	var offset = argStartOffset;
	argDataView.setUint32(offset, argString.length, true); offset += 4;
	for (i = 0; i < argString.length; i++) {
		argDataView.setUint8(offset, argString.charCodeAt(i));
		offset++;
	}
	return offset;
}

Array.prototype.removeIf = function(argPredicate) {
	var idx = this.findIndex(argPredicate);
	if (idx >= 0) {
		this.splice(idx, 1);
	}
}

function setElementsDisplay(argElements, argDisplay) {
	for (i = 0; i < argElements.length; i++) {
		argElements[i].style.display = argDisplay;
	}
}
/*
CHC BOTB Buzzers
		
Brian Charles
May 2020
*/

var SERVER_URL = "ws://192.168.1.253:1988";
//var SERVER_URL = "ws://botb.brianmcharles.com:1988";

var STORAGE_KEY_SESSIONID = "BOTB_BUZZER_SESSIONID_KEY";

// enums (THESE NEED TO MATCH THE SERVER ENUM NUMBERING!!!)
// role types (Client::ClientType)
var ROLE_STUDENT = 1;
var ROLE_FACULTY = 2;
var ROLE_QUESTIONREADER = 3;

// network message types (and in comments whether they're incoming and/or outgoing)
var MSG_PING					= 1; // IN/OUT -request
var MSG_PONG					= 2; // IN/OUT -response
var MSG_CLOCKSYNC				= 3; // IN/OUT
var MSG_DISCONNECT				= 4; // IN/OUT
var MSG_RECONNECT				= 5; // IN/OUT
var MSG_JOINROLE				= 6; // OUT
var MSG_AUTHSTATUS				= 7; // IN
var MSG_BUZZERSTATUS			= 8; // IN
var MSG_BUZZIN					= 9; // OUT
var MSG_BUZZEDIN				= 10; // IN
var MSG_BUZZWINNER				= 11; // IN
var MSG_RESETBUZZERS			= 12; // OUT
var MSG_ADDCONTESTANT			= 13; // IN
var MSG_REMOVECONTESTANT		= 14; // IN
var MSG_UPDATECONTESTANTSTATE	= 15; // IN
var MSG_KICKCLIENT				= 16; // OUT

// DOM elements
var throbberDiv;

var connectButtonDiv;
//
var roleChoiceDiv;

var roleAuthDiv;
var roleAuthRoleNameDiv;
var roleAuthNameDiv;
//
var sharedGameViewDiv;
var contestantTable;
var studentsTableHeader;
var facultyTableHeader;

var contestantViewDiv;
var buzzerButton;

var questionReaderElements;
var resetButton;
//
var disconnectButton;
var statusEntry;
var pingEntry;

// networking
var serverSocket;
var connected = false;
var currentPing = 0; // in ms
var pingArray = [];
var pingArrayIdx = 0;
var pingArraySize = 20; // max number of values to store and average against
var gameClockOffset = 0; // in ms. Add to getTimestamp() to get server time
var lastUpdateTimestamp = 0; // in server time
var isInitialClockSyncDone = false;

// state and stuff
var role;
var clientID;
var canBuzzIn = false;
var nextBuzzSequenceNumber = 0;
// players
function CContestant(argName, argRole, argID) {
	this.name = argName;
	// this is basically their team ID
	this.role = argRole;
	this.clientID =  argID;

	this.isConnected = true;

	this.isBuzzedIn = false;	
	this.buzzInTime = 0;
	this.isBuzzWinner = false;
}

var studentContestants = [];
var facultyContestants = [];

function getContestantByID(argID) {
	var client = studentContestants.find(contestant => contestant.clientID == argID);
	if (client === undefined) {
		client = facultyContestants.find(contestant => contestant.clientID == argID);
	}

	return client;
}

// buzz place numbers to dynamically update
var buzzPlaceCells = [];

function updateBuzzPlaceCells() {
	buzzPlaceCells.sort((a, b) => {return (a.buzzTime < b.buzzTime) ? -1 : 1});
	if (buzzPlaceCells.length > 0) {
		var earliestBuzzTime = buzzPlaceCells[0].buzzTime;
	}

	var buzzPlace = 1;
	buzzPlaceCells.forEach(cell => {
		cell.innerHTML = "<span class='tooltip'>" + buzzPlace++ + "<span class='tooltiptext'>+" + (cell.buzzTime - earliestBuzzTime).toFixed(3) + "s</span></span>";
	});
}

// sounds
var contestantBuzzerSound = new Howl({src: ['contestantBuzzer.mp3']});
var othersBuzzerSound = new Howl({src: ['othersBuzzer.mp3']});
var winnerSound = new Howl({src: ['winnerBell.mp3']});

// network code
function connect() {
	connectButtonDiv.style.display = "none";
	throbberDiv.style.display = "";
	statusEntry.innerHTML = "Connecting...";

	// always disable buzzer on connect. Rely on the server to enable the buzzer
	// this guards against a buzz-in/reconnect/buzz-in-again scenario
	enableBuzzer(false);

	// clear out any old state
	isInitialClockSyncDone = false;
	role = -1;
	clientID = -1;
	studentContestants = [];
	facultyContestants = [];
	studentsTableHeader.classList.remove("winner");
	facultyTableHeader.classList.remove("winner");

	serverSocket = new WebSocket(SERVER_URL);
	serverSocket.binaryType = "arraybuffer";

	// Let the user know we're connected
	serverSocket.onopen = function(evt) {
		connected = true;
		
		disconnectButton.style.display = "";

		// the server will initiate clock synchronization
		statusEntry.innerHTML = "Synchronizing...";		
	};

	// Disconnection occurred.
	serverSocket.onclose = function(evt) {
		if (evt.code == 1006) {
			// abnormal closure
			// this will happen (among other things) when the connection fails to establish (ERR_CONNECTION_REFUSED)
			// or if the server just dies
			//console.log("BMC: Connection closed or refused?");
		}

		connected = false;
		enableBuzzer(false);

		hideAllScreens();
		connectButtonDiv.style.display = "";

		disconnectButton.style.display = "none";
		statusEntry.innerHTML = "Disconnected";
		pingEntry.innerHTML = "---";
	};

	// handle server messages
	serverSocket.onmessage = function(evt) {
		var data = evt.data;

		var messageType = (new Uint8Array(data))[0];
		data = data.slice(1);
		switch (messageType) {
			// *** Networking Messages ***
			// incoming ping request from the server
			case MSG_PING:
				var startTime = (new Float64Array(data))[0]
				var pongMsg = new ArrayBuffer(9);
				var view = new DataView(pongMsg);
				view.setUint8(0, MSG_PONG)
				view.setFloat64(1, startTime, true);
				serverSocket.send(pongMsg);
				break;
				
			// incoming ping response from the server
			case MSG_PONG:
				var startTime = (new Float64Array(data))[0];
				pingArray[pingArrayIdx] = getTimestamp() - startTime;
				pingArrayIdx++;
				if (pingArrayIdx >= pingArraySize) {
					pingArrayIdx = 0;
				}
				currentPing = pingArray.reduce(function(a,b) { return a + b; }) / pingArray.length;
				pingEntry.innerHTML = currentPing.toFixed(0) + " ms";
				break;							
				
			case MSG_CLOCKSYNC:
				var clockSyncData = new DataView(data);
				var done = clockSyncData.getUint8(0);				
				var error = clockSyncData.getFloat64(1, true);
				// the server applied a ping/2 offset before calculating the error so we don't need to
				gameClockOffset += error;

				if (done == 1) {
					if (isInitialClockSyncDone == false) {
						isInitialClockSyncDone = true;
						onConnectedAndSynced();					
					}
				} else {
					var syncMessage = new ArrayBuffer(9);
					var view = new DataView(syncMessage);
					view.setUint8(0, MSG_CLOCKSYNC);
					view.setFloat64(1, getTimestamp() + gameClockOffset, true);
					serverSocket.send(syncMessage);
				}
				break;

			// *** Connecting/Disconnecting/Auth Messages ***
			case MSG_AUTHSTATUS:
				var authStatusData = new DataView(data);
				var authStatus = authStatusData.getUint8(0);

				var isReconnectResponse = (window.localStorage.getItem(STORAGE_KEY_SESSIONID) !== null);
				if (isReconnectResponse) {
					throbberDiv.style.display = "none";
				}

				if (authStatus == 0) {
					if (isReconnectResponse) {
						// if we were trying to reconnect it failed so remove any saved session key
						window.localStorage.removeItem(STORAGE_KEY_SESSIONID);

						// go to role choice like a new user
						roleChoiceDiv.style.display = "";
					} else {
						alert("Incorrect password");
					}
					break;
				}
				// if correct password:
				clientID = authStatusData.getInt32(1, true);
				role = authStatusData.getUint8(5);
				var sessionIDLength = authStatusData.getUint32(6, true);
				// no need to worry about utf8->JS utf-16 or ucs2 since this is a hash that only uses 0-9 and a-f
				var sessionID = String.fromCharCode.apply(null, new Uint8Array(data.slice(10)));
				window.localStorage.setItem(STORAGE_KEY_SESSIONID, sessionID);
				onRoleAuthed();
				break;

			// *** Game Messages ***

			case MSG_BUZZERSTATUS:
				var statusData = new DataView(data);
				var shouldEnableBuzzer = (statusData.getUint8(0) == 1);
				enableBuzzer(shouldEnableBuzzer);
				nextBuzzSequenceNumber = statusData.getUint32(1, true);

				if (shouldEnableBuzzer) {
					// this message does double duty as our "reset the buzzer" notification
					var clearBuzz = function(contestant) {
						contestant.isBuzzedIn = false;
						contestant.buzzInTime = 0;
						contestant.isBuzzWinner = false;

						var tableCell = document.getElementById("clientCell" + contestant.clientID);
						tableCell.classList.remove("buzzedIn");
						tableCell.classList.remove("winner");						
					};
					studentContestants.forEach(clearBuzz);
					facultyContestants.forEach(clearBuzz);

					studentsTableHeader.classList.remove("winner");
					facultyTableHeader.classList.remove("winner");

					if (role == ROLE_QUESTIONREADER) {
						resetButton.disabled = false;

						buzzPlaceCells.forEach(cell => cell.innerHTML = "");
						buzzPlaceCells = [];
					}					
				}
				break;

			case MSG_BUZZEDIN:
				var buzzInData = new DataView(data);
				var buzzClientID = buzzInData.getInt32(0, true);
				var buzzTime = buzzInData.getFloat64(4, true);
				
				var buzzClient = getContestantByID(buzzClientID);
				if (buzzClient === undefined) {
					// Not necessarily a bug. Could just be that the client left the game
					console.log("unable to process buzz in. unknown client id " + buzzClientID)
					break;
				}
				buzzClient.isBuzzedIn = true;
				buzzClient.buzzInTime = buzzTime;

				if (buzzClientID != clientID) {
					othersBuzzerSound.play();
				} else {
					// this is required for reconnecting clients that have buzzed in so they don't buzz a second time
					enableBuzzer(false);
				}
				
				document.getElementById(("clientCell" + buzzClientID)).classList.add("buzzedIn");

				if (role == ROLE_QUESTIONREADER) {
					var placeCell = document.getElementById("buzzPlaceCell" + buzzClientID);
					placeCell.buzzTime = buzzTime;
					buzzPlaceCells.push(placeCell);
					updateBuzzPlaceCells();
				}
				break;

			case MSG_BUZZWINNER:
				var buzzWinData = new DataView(data);
				var buzzWinClientID = buzzWinData.getInt32(0, true);
				var buzzWinClientRole = buzzWinData.getUint8(4);

				winnerSound.play();

				if (buzzWinClientRole== ROLE_STUDENT) {
					studentsTableHeader.classList.add("winner");
				} else if (buzzWinClientRole == ROLE_FACULTY) {
					facultyTableHeader.classList.add("winner");
				}

				var buzzWinner = getContestantByID(buzzWinClientID);
				if (buzzWinner === undefined) {
					// Not necessarily a bug. Could just be that the client left the game
					console.log("unable to fully process buzz win. unknown client id " + buzzWinClientID)
					break;
				}
				buzzWinner.isBuzzWinner = true;				
				document.getElementById(("clientCell" + buzzWinClientID)).classList.add("winner");
				break;

			case MSG_ADDCONTESTANT:
				var contestantData = new DataView(data);
				var contestantID = contestantData.getInt32(0, true);
				var contestantRole = contestantData.getUint8(4, true);
				var contestantNameLength = contestantData.getUint32(5, true);
				var contestantName = String.fromCharCode.apply(null, new Uint8Array(data.slice(9)));
				contestantName = utf8.decode(contestantName);

				var contestant = new CContestant(contestantName, contestantRole, contestantID);
				if (contestantRole == ROLE_STUDENT) {
					studentContestants.push(contestant);
				} else if (contestantRole == ROLE_FACULTY) {
					facultyContestants.push(contestant);
				} else {
					console.log("Recieved MSG_ADDCONTESTANT with invalid role id: " + contestantRole);
				}

				// todo: make this more efficient (i.e. nicely just add the needed table elt(s))
				rebuildContestantTable();
				break;

			case MSG_REMOVECONTESTANT:
				var contestantData = new DataView(data);
				var contestantID = contestantData.getInt32(0, true);
				
				studentContestants.removeIf(contestant => contestant.clientID == contestantID);
				facultyContestants.removeIf(contestant => contestant.clientID == contestantID);

				// todo: make this more efficient (i.e. nicely just add the needed table elt(s))
				rebuildContestantTable();
				break;

			case MSG_UPDATECONTESTANTSTATE:
				var contestantStateData = new DataView(data);
				var contestantID = contestantStateData.getInt32(0, true);
				var isContestantNowConnected = (contestantStateData.getUint8(4) == 1);
				var newContestantID = contestantStateData.getInt32(5, true);

				var contestant = getContestantByID(contestantID);
				contestant.isConnected = isContestantNowConnected;
				contestant.clientID = newContestantID;
				var tableCell = document.getElementById(("clientCell" + contestantID));
				tableCell.id = ("clientCell" + newContestantID);

				if (isContestantNowConnected) {					
					tableCell.classList.remove("disconnected");					
				} else {
					tableCell.classList.add("disconnected");
				}
				
				break;
				
			default:
				console.log("Error decoding message with ID :" + messageType);
		}
	};
}

// locally-initiated disconnection event
function disconnect() {
	if (serverSocket && serverSocket.readyState == WebSocket.OPEN) {
		var disconnectMessage = new ArrayBuffer(1);
		var view = new DataView(disconnectMessage);
		view.setUint8(0, MSG_DISCONNECT);
		serverSocket.send(disconnectMessage);

		serverSocket.close();
	}

	window.localStorage.removeItem(STORAGE_KEY_SESSIONID);
}

// if connected, send a ping request to the server
function sendPing() {
	if (!connected || serverSocket.readyState != WebSocket.OPEN) {
		return;
	}
		
	var pingMsg = new ArrayBuffer(9);
	var view = new DataView(pingMsg);
	view.setUint8(0, MSG_PING);
	view.setFloat64(1, getTimestamp(), true);
	serverSocket.send(pingMsg);
}

// Game logic
function onConnectedAndSynced() {
	statusEntry.innerHTML = "Connected";	

	// should we attempt to reconnect?
	var sessionID = window.localStorage.getItem(STORAGE_KEY_SESSIONID);
	if (sessionID !== null) {
		var messageLength = 5; // msgid and session string length
		messageLength += sessionID.length;

		var reconnectMsg = new ArrayBuffer(messageLength);
		var view = new DataView(reconnectMsg);
		view.setUint8(0, MSG_RECONNECT);
		addUTF8StringToDataViewAtOffset(sessionID, view, 1);
		serverSocket.send(reconnectMsg);
	} else {
		throbberDiv.style.display = "none";

		// show role choice
		roleChoiceDiv.style.display = "";
	}
}

function chooseRole(argRole) {
	// perhaps not the most elegant way to validate input but meh it works and I have a deadline here
	switch(argRole) {
		case ROLE_STUDENT:
			roleAuthRoleNameDiv.innerHTML = "Student";		
			break;

		case ROLE_FACULTY:
			roleAuthRoleNameDiv.innerHTML = "Faculty/Alumni";
			break;
		
		case ROLE_QUESTIONREADER:
			roleAuthRoleNameDiv.innerHTML = "Question Reader";
			break;

		default:
			console.log("invalid role choice!");
			return;
	}

	role = argRole;
	roleChoiceDiv.style.display = "none";

	var needsName = (role == ROLE_STUDENT || role == ROLE_FACULTY);	
	roleAuthNameDiv.style.display = needsName ? "" : "none";
	roleAuthDiv.style.display = "";
	document.getElementById("password").value = "";
}

function goBackToRoleChoice() {
	role = "";
	roleChoiceDiv.style.display = "";
	roleAuthDiv.style.display = "none";
}

function authRole() {
	var needsName = (role == ROLE_STUDENT || role == ROLE_FACULTY);	
	var password = document.getElementById("password").value;

	// messageID + roleID
	var messageLength = 2;

	// first check that the password is there
	if (password.length <= 0) {
		alert("Please enter a password.");
		return;		
	}

	// this is a super basic anti-cheat measure, so we're not going to bother with hashing or anything
	var passwordUTF8 = utf8.encode(password);
	messageLength += 4 + passwordUTF8.length;

	// if we need a name check if that is there
	var nameUTF8;
	if (needsName) {
		var name = document.getElementById("name").value;
		if (name.length <= 0) {
			alert("Please enter a name.");
			return;
		}

		nameUTF8 = utf8.encode(name);
		messageLength += 4 + nameUTF8.length;
	}

	var joinMessage = new ArrayBuffer(messageLength);
	var view = new DataView(joinMessage);
	var offset = 0;
	view.setUint8(offset, MSG_JOINROLE); offset += 1;
	view.setUint8(offset, role); offset += 1;
	offset = addUTF8StringToDataViewAtOffset(passwordUTF8, view, offset);
	if (needsName) {
		offset = addUTF8StringToDataViewAtOffset(nameUTF8, view, offset);
	}
	serverSocket.send(joinMessage);
}

function onRoleAuthed() {
	roleAuthDiv.style.display = "none";

	sharedGameViewDiv.style.display = "";
	rebuildContestantTable();
	
	switch(role) {
		case ROLE_STUDENT:
		case ROLE_FACULTY:
			contestantViewDiv.style.display = "";
			break;
		
		case ROLE_QUESTIONREADER:
			setElementsDisplay(questionReaderElements, "");
			break;
	}
}

window.addEventListener("keydown", function(event) {
	if (event.code == "Space") {
		buzzIn();
	}
});

function enableBuzzer(argEnabled) {
	canBuzzIn = argEnabled;
	buzzerButton.disabled = !argEnabled;
}

// when you trigger your buzzer
function buzzIn() {
	if (canBuzzIn == false) {
		return;
	}
	if (role == ROLE_QUESTIONREADER) {
		return;
	}

	var gameClock = (getTimestamp() + gameClockOffset) / 1000;

	var buzzMessage = new ArrayBuffer(13);
	var view = new DataView(buzzMessage);
	view.setUint8(0, MSG_BUZZIN);
	view.setFloat64(1, gameClock, true);
	view.setUint32(9, nextBuzzSequenceNumber, true);
	serverSocket.send(buzzMessage);

	enableBuzzer(false);
	contestantBuzzerSound.play();

	buzzerButton.classList.add("popOnce");
	setTimeout(function() {
		buzzerButton.classList.remove("popOnce");
	}, 250);
}

// when you, a question reader, reset the system for the next question
function resetBuzzers() {
	resetButton.disabled = true;

	var resetMessage = new ArrayBuffer(1);
	var view = new DataView(resetMessage);
	view.setUint8(0, MSG_RESETBUZZERS);
	serverSocket.send(resetMessage);
}

// when you, a question reader, want to kick a client
function kick(argClientID) {
	var kickMessage = new ArrayBuffer(5);
	var view = new DataView(kickMessage);
	view.setUint8(0, MSG_KICKCLIENT);
	view.setInt32(1, argClientID, true);
	serverSocket.send(kickMessage);
}

function rebuildContestantTable() {
	// remove all rows except the first (the header)
	while (contestantTable.rows.length > 1) {
		contestantTable.deleteRow(-1);
	}

	buzzPlaceCells = []

	var rowIdx = 0;
	while(true) {
		var student = studentContestants[rowIdx];
		var faculty = facultyContestants[rowIdx];
		if (student === undefined && faculty === undefined) {
			break;
		}

		var row = contestantTable.insertRow(-1);
		var colIdx = 0;
		var insertTD = function(contestant) {
			if (contestant !== undefined) {
				var cell = row.insertCell(colIdx++);
				cell.innerHTML = contestant.name;
				cell.id = ("clientCell" + contestant.clientID);

				if (contestant.clientID == clientID) {
					cell.classList.add("you");
				}
				if (contestant.isConnected == false) {
					cell.classList.add("disconnected");
				}
				if (contestant.isBuzzedIn) {
					cell.classList.add("buzzedIn");
				}
				if (contestant.isBuzzWinner) {
					cell.classList.add("winner");
				}
			} else {
				// empty cell
				row.insertCell(colIdx++);				
			}
		}
		
		// these next two are for question readers' additional displays and controls
		var insertBuzzPlaceTD = function(contestant) {
			if (contestant !== undefined) {
				var cell = row.insertCell(colIdx++);
				cell.classList.add("buzzPlaceCell");
				cell.id = ("buzzPlaceCell" + contestant.clientID);

				if (contestant.isBuzzedIn) {
					cell.buzzTime = contestant.buzzInTime;
					buzzPlaceCells.push(cell);
				}
			} else {
				// empty cell
				row.insertCell(colIdx++);				
			}
		}

		var insertKickTD = function(contestant, buttonText) {
			if (contestant !== undefined) {
				var cell = row.insertCell(colIdx++);
				cell.innerHTML = "<button type='button' onclick='kick(" + contestant.clientID + ");'>" + buttonText + "</button>";				
			} else {
				// empty cell
				row.insertCell(colIdx++);				
			}
		}

		if (role == ROLE_QUESTIONREADER) {
			insertBuzzPlaceTD(student);
		}
		insertTD(student);
		if (role == ROLE_QUESTIONREADER) {
			// kick buttons
			insertKickTD(student, "&lt;kick");
			insertKickTD(faculty, "kick&gt;");
		}
		insertTD(faculty);
		if (role == ROLE_QUESTIONREADER) {
			insertBuzzPlaceTD(faculty);
		}

		rowIdx++;
	}

	updateBuzzPlaceCells();
}

// ok maybe I should've looked into some kind of framework for this
// at least some basic state machine?
function hideAllScreens() {
	throbberDiv.style.display = "none";
	connectButtonDiv.style.display = "none";
	roleChoiceDiv.style.display = "none";
	roleAuthDiv.style.display = "none";
	sharedGameViewDiv.style.display = "none";
	contestantViewDiv.style.display = "none";
	setElementsDisplay(questionReaderElements, "none");
}

function init() {
	throbberDiv = document.getElementById("throbber");

	connectButtonDiv = document.getElementById("connectButton");

	roleChoiceDiv = document.getElementById("roleChoice");

	roleAuthDiv = document.getElementById("roleAuth");
	roleAuthRoleNameDiv = document.getElementById("roleAuthRoleName");
	roleAuthNameDiv = document.getElementById("roleAuthName");

	sharedGameViewDiv = document.getElementById("sharedGameView");
	contestantTable = document.getElementById("contestantTable");
	studentsTableHeader = document.getElementById("studentsHeader");
	facultyTableHeader = document.getElementById("facultyHeader");

	contestantViewDiv = document.getElementById("contestantView");
	buzzerButton = document.getElementById("buzzerButton");

	questionReaderElements = document.getElementsByClassName("questionReaderElement");
	resetButton = document.getElementById("resetButton");

	disconnectButton = document.getElementById("disconnectButton");	
	statusEntry = document.getElementById("statusEntry");
	pingEntry = document.getElementById("pingEntry");	

	// ping the server at 5Hz
	window.setInterval(sendPing, 200);

	// used for debugging clocksync
	// var onFrame = function() {
	// 	var time = (getTimestamp() + gameClockOffset) / 1000;
	// 	statusEntry.innerHTML = time.toFixed(3);

	// 	animate(onFrame);
	// };
	// animate(onFrame);

	connect();
}
